import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  private url = environment.baseUrl + "/request";

  constructor(private http?: HttpClient) { }

  filter(data) {
    return this.http.post(this.url, data)
  }

  detail(id) {
    return this.http.get(this.url + "/detail/" + id)
  }

  update(data) {
    return this.http.post(this.url + "/update/", data)
  }

  unseen() {
    return this.http.get(this.url + "/unseen")
  }

}
 