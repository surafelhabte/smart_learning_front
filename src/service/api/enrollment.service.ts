import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EnrollmentService {

  private url = environment.baseUrl;

  constructor(private http?: HttpClient) { }

   create(data): any {
    return this.http.post(this.url + "/enrollment", data)
   }

   update(data) {
    return this.http.put(this.url + '/enrollment/update', data)
  }

   filter(data) {
    return this.http.post(this.url + "/enrollment/filter",data)
   }

   get(id) {
    return this.http.get(this.url + '/enrollment/get/' + id)
   }

   detail(id) {
    return this.http.get(this.url + '/enrollment/detail/' + id)
   }

   programs() {
    return this.http.get(this.url + '/enrollment/programs')
   }

   employees(data) {
    return this.http.post(this.url + '/enrollment/employees',data)
   }

  delete(id) {
    return this.http.delete(this.url + id)
  }

}
