import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  private url = environment.baseUrl;

  constructor(private http?: HttpClient) { }

   create(data): any {
    return this.http.post(this.url + "/program", data)
   }

   update(data) {
    return this.http.put(this.url + '/program/update', data)
  }

   filter(data) {
    return this.http.post(this.url + "/program/filter",data)
   }

   get(id) {
    return this.http.get(this.url + '/program/get/' + id)
   }

   detail(id) {
    return this.http.get(this.url + '/program/detail/' + id)
   }

   training() {
    return this.http.get(this.url + '/program/Trainings')
   }

  provider() {
  return this.http.get(this.url + '/provider')
  }

  delete(id) {
    return this.http.delete(this.url + id)
  }
  
}
 