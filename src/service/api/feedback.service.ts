import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  private url = environment.baseUrl + "/feedback";

  constructor(private http?: HttpClient) { }

  create(data): any {
    return this.http.post(this.url, data)
  }

  gets(id) {
    return this.http.get(this.url + "/" + id)
  }

  delete(id) {
    return this.http.delete(this.url + "/" + id)
  }

} 
