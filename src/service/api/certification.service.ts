import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CertificationService {
  // id, enrollment_id, result, date, training, attchement, employee_id, 

  private url = environment.baseUrl;

  constructor(private http?: HttpClient) { }

  create(data): any {
   return this.http.post(this.url + "/certification", data)
  }

  update(data) {
   return this.http.put(this.url + '/certification/update', data)
  }

  filter(data) {
   return this.http.post(this.url + "/certification/filter",data)
  }

  get(id) {
   return this.http.get(this.url + '/certification/get/' + id)
  }

  enrollment(data) {
    return this.http.post(this.url + '/certification/Enrollments',data)
  }

  employee(data) {
   return this.http.post(this.url + '/certification/employee',data)
  }

  delete(id) {
   return this.http.delete(this.url + id)
  }

}
  