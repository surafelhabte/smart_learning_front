import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  private url = environment.baseUrl + "/report";

  constructor(private http?: HttpClient) { }

  dashboard() {
    return this.http.get(this.url + "/dashboard")
  }

 
}
 