import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private url = environment.baseUrl + '/provider';

  constructor(private http?: HttpClient) { }

   create(data): any {
    return this.http.post(this.url, data)
   }

   gets() {
    return this.http.get(this.url)
   }

   get(id) {
    return this.http.get(this.url + '/get/' + id)
   }

   update(data) {
    return this.http.put(this.url + '/update', data)
  }

  delete(id) {
    return this.http.delete(this.url + id)
  }

}
 