import { SecurityService } from './security.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TokenizeService {

  constructor(private service: SecurityService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const key = this.service.logged_in_data.key;

    if (key) {
      const cloned = req.clone({
              headers: req.headers.set('Authorization', key)
          });
      return next.handle(cloned);

    } else {
      const cloned = req.clone({
        headers: req.headers.set('Authorization', '')
    });
      return next.handle(cloned);
    }
  }
  
}
