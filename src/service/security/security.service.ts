import { environment } from './../../environments/environment';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { RequestService } from '../api/request.service';

@Injectable({
  providedIn: "root"
})
export class SecurityService {
  public roles: any[] = [];
  public logged_in_data: any = null;
  public url: any = environment.baseUrl;
  public unseen_requests: any;

  constructor(private http: HttpClient,private router: Router, public req_service: RequestService) {}

  public hasClaim(role: any): boolean {
    let result = false;

    this.roles.forEach(element => {
      if(element.claimType === role){
        if(element.claimValue === true){
          result = true;
        }
      }
    });

    if(result){
      return true;

    } else {
      return false;

    }
  
  }

  public logOut() {
    this.http.get(this.url + '/util/util/Logout').subscribe((data: any) => {
      alert('logged out successfully');
    });
    
  }

  public get_logged_in_data(){
    this.http.get(this.url + '/util/util/Is_logged_in').subscribe((data: any) => {
      this.logged_in_data = data;
    });

  }

  public get_roles(){
    this.http.get(this.url + '/util/util/Roles').subscribe((data: any) => {
      this.roles = data;
    });

  }

  public get_unseen_request(){
    this.req_service.unseen().subscribe((data: any) => {
      this.unseen_requests = data;
    });
  }


}
