import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { SecurityService } from './security.service';
import { Observable } from 'rxjs';

@Injectable()

export class AuthGuard implements CanActivate {
    constructor(private router: Router, private service: SecurityService) {}
  
    canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean | UrlTree> | boolean | UrlTree {
        const role: string = next.data["role"];
        
        if (this.service.hasClaim(role)) {
            return true;

        } else {
            return false;

        }
    }
  }