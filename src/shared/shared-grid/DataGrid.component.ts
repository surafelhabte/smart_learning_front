import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from "@angular/core";
import {
  PageSettingsModel,
  SearchSettingsModel,
  TextWrapSettingsModel,
  EditSettingsModel,
  CommandModel,
  GridComponent,
  IRow,
  Column
} from "@syncfusion/ej2-angular-grids";
import { closest } from "@syncfusion/ej2-base";
import { ClickEventArgs } from "@syncfusion/ej2-angular-navigations";
import { SecurityService } from 'src/service/security/security.service';

@Component({
  selector: "app-data-grid",
  templateUrl: "./DataGrid.component.html",
  styleUrls: ["./DataGrid.component.css"]
})
export class DataGridComponent implements OnInit {
  @Input() gridData: any;
  @Input() allowOperation: any;
  @Input() allowToolbar: any;
  @Input() searchField: any;
  @Input() incomingCommand: any[] = [];
  @Input() columns: any[] = [];
  @Input() grouped: boolean;
  @Input() groupOptions: any;
  @Input() height: any;
  @Input() allowExporttoExcel: boolean;
  @Input() showAdd = false;
  @Input() showExcelExport = false;
  @Input() showPrint = false;
  @Input() showSearch = false;
  @Input() showFilter = false;
  @Input() incomingToolbar: any;
  @Input() claimType: any;

  @Output() public addRecord: EventEmitter<any> = new EventEmitter();
  @Output() public editRecord: EventEmitter<any> = new EventEmitter();
  @Output() public deleteRecord: EventEmitter<any> = new EventEmitter();
  @Output() public detailRecord: EventEmitter<any> = new EventEmitter();

  public pageSettings: PageSettingsModel;
  public toolbar: Array<any>;
  public searchOptions: SearchSettingsModel;

  public wrapSettings: TextWrapSettingsModel;
  public editSettings: EditSettingsModel;
  public commands: CommandModel[];
  public onfilter = false;

  @ViewChild("grid")
  public grid: GridComponent;
  public pageSizes = ["10", "50", "100", "200", "All"];

  constructor(public service: SecurityService) {}

  ngOnInit() {
    this.pageSettings = { pageSize: 10, pageSizes: this.pageSizes };
    this.wrapSettings = { wrapMode: "Content" };
    this.toolbar = this.incomingToolbar;

    this.searchOptions = {
      operator: "contains",
      key: "",
      ignoreCase: true
    };
    this.editSettings = { allowAdding: true };

    if (this.incomingCommand.length > 0) {
      this.incomingCommand.forEach(element => {
        switch(element){
          case 'edit' && this.service.hasClaim("canUpdate" + this.claimType):
          this.commands.push(
          { 
            type: "Edit",
            buttonOption: {cssClass: "e-flat", iconCss: "e-edit e-icons",click: this.onEdit.bind(this)}
          });
          break;  
          case 'delete' && this.service.hasClaim("canDelete" + this.claimType):
          this.commands.push(
          {
            type: "Delete",
            buttonOption: {cssClass: "e-flat", iconCss: "e-delete e-icons",click: this.onDelete.bind(this)}
          });
          break;  
          case 'detail' && this.service.hasClaim("canViewDetail" + this.claimType):
          this.commands.push(
          {
            type: "Edit",
            buttonOption: {cssClass: "e-flat", iconCss: "e-preview e-icons",click: this.onDetail.bind(this)}
          });
          break;  
        }
      });
    }
    this.initializeToolBar();
  }

  initializeToolBar(): void {
    this.toolbar = [];
    if (this.showAdd && this.service.hasClaim("canAdd" + this.claimType)) {
      this.toolbar.push({ text: "Add", prefixIcon: "e-create"});
    }
    
    if (this.showFilter) {
      this.toolbar.push({
        text: "Filter",
        prefixIcon: "e-BT_Excelfilter"
      });
    }
    if (this.showExcelExport) {
      this.toolbar.push({
        text: "ExcelExport",
        prefixIcon: "e-Excel_Export"
      });
    }
    if (this.showPrint) {
      this.toolbar.push({
        text: "Print",
        prefixIcon: "e-print"
      });
    }

    if (this.showSearch) {
      this.toolbar.push({
        text: "Search",
        tooltipText: "Search Items"
      });
    }
  }

  onEdit(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, ".e-row").getAttribute("data-uid")
    );
    const data1: any = rowObj.data;
    this.editRecord.emit(data1);
  }

  onDelete(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, ".e-row").getAttribute("data-uid")
    );
    const data1: any = rowObj.data;
    this.deleteRecord.emit(data1);
  }

  onDetail(args: Event): void {
    const rowObj: IRow<Column> = this.grid.getRowObjectFromUID(
      closest(args.target as Element, ".e-row").getAttribute("data-uid")
    );
    const data1: any = rowObj.data;
    this.detailRecord.emit(data1);
  }

  toolbarClicked(args: ClickEventArgs): void {
    switch (args.item.text) {
      case "Add":
      this.addRecord.emit(args.item.id);
      break;
      case "Filter":
      if (this.onfilter) {
        this.grid.allowFiltering = false;
        this.onfilter = !this.onfilter;
      } else {
        this.grid.allowFiltering = true;
        this.onfilter = !this.onfilter;
      }
      break;
    
      case "ExportExcel":
      this.grid.excelExport();
      break;
      case "Print":
      this.grid.print();
      break;
    }
  }
}
