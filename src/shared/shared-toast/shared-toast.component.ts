import { Component, ViewChild } from "@angular/core";

@Component({
  selector: "app-shared-toast",
  templateUrl: "./shared-toast.component.html",
  styleUrls: ["./shared-toast.component.css"]
})
export class SharedToastComponent {
  @ViewChild("element") element;
  public position = { X: "Center", Y: "Top" };

  Show(data: any) {
    this.element.show(data);
    history.state.data = undefined;
  }

  onClick(e) {
    e.clickToClose = true;
  }
}
