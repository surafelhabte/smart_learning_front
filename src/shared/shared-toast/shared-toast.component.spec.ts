import { SharedToastComponent } from "./shared-toast.component";
import {
  ComponentFixture,
  TestModuleMetadata,
  TestBed,
  async
} from "@angular/core/testing";
import { DebugElement } from "@angular/core";
import { RouterTestingModule } from "@angular/router/testing";
import { ToastModule } from "@syncfusion/ej2-angular-notifications";

describe("SharedToastComponent", () => {
  let component: SharedToastComponent;
  let fixture: ComponentFixture<SharedToastComponent>;
  let debugEl: DebugElement;

  const makeCompiledTestBed = (provider?: object): void => {
    const moduleDef: TestModuleMetadata = {
      imports: [RouterTestingModule, ToastModule],
      declarations: [SharedToastComponent]
    };
    if (moduleDef.providers && provider) {
      moduleDef.providers.push(provider);
    }
    TestBed.configureTestingModule(moduleDef).compileComponents();
  };

  const setupTestVars = (): void => {
    fixture = TestBed.createComponent(SharedToastComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();
  };
  beforeEach(async(makeCompiledTestBed));
  beforeEach(setupTestVars);

  it("should create ", () => {
    const fixture = TestBed.createComponent(SharedToastComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
