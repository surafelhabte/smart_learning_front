import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import {
  DropDownListModule,
  MultiSelectModule
} from '@syncfusion/ej2-angular-dropdowns';
import { CheckBoxModule } from '@syncfusion/ej2-angular-buttons';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import {
  TabModule,
  SidebarModule,
  TreeViewModule} from '@syncfusion/ej2-angular-navigations';
import { TokenizeService } from 'src/service/security/tokenize.service';
import { DatePickerModule } from "@syncfusion/ej2-angular-calendars";
import { ToastModule } from "@syncfusion/ej2-angular-notifications";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DropDownListModule,
    TextBoxModule,
    ButtonModule,
    DatePickerModule,
    CheckBoxModule,
    NumericTextBoxModule,
    MultiSelectModule,
    GridModule,
    TabModule,
    ToastModule,
    SidebarModule,
    TreeViewModule, 
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DropDownListModule,
    TextBoxModule,
    ButtonModule,
    DatePickerModule,
    CheckBoxModule,
    NumericTextBoxModule,
    MultiSelectModule,
    GridModule,
    TabModule,
    ToastModule,
    SidebarModule,
    TreeViewModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenizeService, multi: true }
  ]
})
export class SharedModule {}
