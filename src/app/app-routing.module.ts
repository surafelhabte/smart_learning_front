import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from '../not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/ws',
    pathMatch: 'full'
  },
  {
    path: 'ws',
    loadChildren: () =>
      import('../../src/featured/workspace/workspace.module').then(
        m => m.WorkspaceModule
      )
  },
  { 
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
