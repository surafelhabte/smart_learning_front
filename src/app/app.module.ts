import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from '../not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { SecurityService } from 'src/service/security/security.service';
import { AuthGuard } from 'src/service/security/AuthGarde';
@NgModule({
  declarations: [AppComponent, NotFoundComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  exports: [],
  providers: [AuthGuard,SecurityService],
  bootstrap: [AppComponent]
})
export class AppModule {}
