import { Component } from '@angular/core';
import { SecurityService } from 'src/service/security/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private service: SecurityService) {
    if(this.service.logged_in_data === null){
      this.service.get_logged_in_data();
    }
    
    if(this.service.roles.length > 0){
      this.service.get_roles();
    }

   }

}
