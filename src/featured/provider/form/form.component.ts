import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProviderService } from 'src/service/api/provider.service';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { Location } from "@angular/common";


@Component({ 
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;

  public formGroup: FormGroup;
  public id: any;
  public formSubmitted = false;
  public isUpdate: boolean = false;

  constructor(private activatedRoute: ActivatedRoute,private service: ProviderService, private location: Location,
    private router: Router) {
    this.formGroup = new FormGroup({ 
      id : new FormControl(null),
      name : new FormControl('',Validators.required),
      email : new FormControl('',[Validators.email]),
      phone : new FormControl('',Validators.required),
      website : new FormControl(''),
      address : new FormControl('',Validators.required),
      logo : new FormControl('')
    })
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id !== undefined) {
      this.isUpdate = true;
      this.service.get(this.id).subscribe((data: any) => {
        this.formGroup.setValue(data)
      });
    }
  }

  public getControls(name): FormControl {
    return this.formGroup.get(name) as FormControl;
  }

  onLogoChange(event) {
    if (event.target.files.length > 0) {
      const logo = event.target.files[0];
      this.getControls('logo').setValue(logo);
    }
  }

  Submit() {
    this.formSubmitted = true;
    if (!this.formGroup.valid) {
      return;

    } else {
      if (this.isUpdate) {
        this.service.update(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/provider'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      } else {
        this.service.create(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/provider'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      }
    }
  }

  cancel() {
    this.location.back();
  }

}
