import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ProviderService } from 'src/service/api/provider.service';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;
  
  public data = [];
  public columns: any[] = [];
  public Command = ['edit', 'delete'];

  constructor(private service: ProviderService, private router: Router) {
    this.prepare();
  }

  ngOnInit() {
    this.service.gets().subscribe((data: any[]) => {
      this.data = data;
    });
  }

  add() {
    this.router.navigate(["provider/create"]);
  }

  edit($event) {
    this.router.navigate(["provider/edit/" + $event.id]);
  }

  delete($event) {
    this.service.delete($event.id).subscribe((response: any) => {
      if (response.status) {
        this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
        this.ngOnInit();
        
      } else {
        this.notif.Show({title: "Error",content: response.message,cssClass: "e-toast-danger" });
          
      }
    });
  }

  prepare() {
    this.columns.push({field: "name",headerText: "Name", textAlign: "center",width: 50 });
    this.columns.push({field: "phone",headerText: "Phone Number", textAlign: "center",width: 30 });
    this.columns.push({field: "email",headerText: "Email Address ", textAlign: "center",width: 30 });
    this.columns.push({field: "website",headerText: "Website", textAlign: "center",width: 30 });
 
  } 

  
}
