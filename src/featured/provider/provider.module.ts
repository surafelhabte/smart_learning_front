import { NgModule } from '@angular/core';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { ProviderService } from 'src/service/api/provider.service';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/shared/Shared.module';
import { AuthGuard } from 'src/service/security/AuthGarde';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title: 'Training Provider', breadCrum: 'View', role: 'canViewProvider' },
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: FormComponent,
    data: { title: 'Training Provider', breadCrum: 'Create', role: 'canAddProvider' },
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: { title: 'Training Provider', breadCrum: 'Edit', role: 'canUpdateProvider' },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [FormComponent,ListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    ProviderService
  ],
  exports: [RouterModule]
})
export class ProviderModule { }
