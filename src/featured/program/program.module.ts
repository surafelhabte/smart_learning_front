import { NgModule } from '@angular/core';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SharedModule } from 'src/shared/Shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ProgramService } from 'src/service/api/program.service';
import { AuthGuard } from 'src/service/security/AuthGarde';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title: 'Program', breadCrum: 'View', role: 'canViewProgram' },
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: FormComponent,
    data: { title: 'Program', breadCrum: 'Create', role: 'canAddProgram' },
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: { title: 'Program', breadCrum: 'Edit', role: 'canUpdateProgram' },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [FormComponent,ListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    ProgramService
  ],
  exports: [RouterModule]
})
export class ProgramModule { }
 