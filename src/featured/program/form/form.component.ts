import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgramService } from 'src/service/api/program.service';
import { Location } from "@angular/common";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;

  public formGroup: FormGroup;
  public id: any;
  public formSubmitted = false;
  public isUpdate: boolean = false;

  public training: any;
  public training_providers: any;
  public fields: any = { text: 'name',value: 'id' };

  constructor(private activatedRoute: ActivatedRoute,private service: ProgramService, private location: Location,
    private router: Router) {
    this.formGroup = new FormGroup({ 
      id : new FormControl(null),
      fiscal_year : new FormControl('',Validators.required),
      cost : new FormControl('',Validators.required),
      start_date : new FormControl('',Validators.required),
      end_date : new FormControl('',Validators.required),
      training_id : new FormControl('',Validators.required),
      training_provider_id : new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
    this.load_training();
    this.load_training_provider();
    
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id !== undefined) {
      this.isUpdate = true;
      this.service.get(this.id).subscribe((data: any) => {
        this.formGroup.setValue(data)
      });
    }
  }

  public getControls(name): FormControl {
    return this.formGroup.get(name) as FormControl;
  }

  Submit() {
    this.formSubmitted = true;
    if (!this.formGroup.valid) {
      return;

    } else {
      if (this.isUpdate) {
        this.service.update(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/program'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      } else {
        this.service.create(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/program'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      }
    }
  }

  load_training(){
    this.service.training().subscribe((data: any) => {
      this.training = data;
    });
  }

  load_training_provider(){
    this.service.provider().subscribe((data: any) => {
      this.training_providers = data;
    });
  }

  cancel() {
    this.location.back();
  }

}