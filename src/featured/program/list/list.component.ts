import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { ProgramService } from 'src/service/api/program.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;
  
  public formGroup: FormGroup;

  public data = [];
  public columns: any[] = [];
  public Command = ['edit', 'delete'];

  public placeholder: any = "-- Select Year to Filter Program --";

  constructor(private service: ProgramService, private router: Router) {
    this.formGroup = new FormGroup({ fiscal_year: new FormControl(null) });
    this.prepare();
  }
  
  ngOnInit() {
    this.load_data(null);
  }

  add() {
    this.router.navigate(["program/create"]);
  }

  edit($event) {
    this.router.navigate(["program/edit/" + $event.id]);
  }

  delete($event) {
    this.service.delete($event.id).subscribe((response: any) => {
      if (response.status) {
        this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
        this.ngOnInit();
        
      } else {
        this.notif.Show({title: "Error",content: response.message,cssClass: "e-toast-danger" });
          
      }
    });
  }

  load_data(value){
   value !== null ? this.formGroup.get('fiscal_year').setValue(value.itemData.value) : null;

   this.service.filter(this.formGroup.value).subscribe((data: any[]) => {
    this.data = data;
   });

  }

  prepare() {
    this.columns.push({field: "training",headerText: "Training", textAlign: "center",width: 70 });
    this.columns.push({field: "fiscal_year",headerText: "Fiscal Year", textAlign: "center",width: 50 });
    this.columns.push({field: "cost",headerText: "Cost", textAlign: "center",width: 30 });
    this.columns.push({field: "schedule",headerText: "Schedule ", textAlign: "center",width: 50 });
    this.columns.push({field: "Training Provider",headerText: "Training Provider", textAlign: "center",width: 50 });
  } 
  
}
 