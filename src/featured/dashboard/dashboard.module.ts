import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/service/security/AuthGarde';
import { DashboardComponent } from './dashboard.component';
import { ReportService } from 'src/service/api/report.service';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: { title: 'View', breadCrum: 'Dashboard', role: 'canViewDashboard' },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [DashboardComponent],
  providers: [ReportService],
  imports: [CommonModule, RouterModule.forChild(routes)]
})
export class DashboardModule {}