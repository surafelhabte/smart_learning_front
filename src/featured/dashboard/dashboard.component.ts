import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/service/api/report.service';

@Component({
  selector: 'smart-recruitment-frontend-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(private service: ReportService) {}

  dashboardItems: {
    name: string;
    class: string;
    icon: string;
    value: number;
  }[] = [
    {
      name: 'Learning Request',
      class: 'request',
      icon: '',
      value: 0
    },
    {
      name: 'Certificate',
      class: 'certificate',
      icon: '',
      value: 0
    },
    {
      name: 'Open programs',
      class: 'program',
      icon: '',
      value: 0
    },
    {
      name: 'Enrollment',
      class: 'enrollment',
      icon: '',
      value: 0
    },
    {
      name: 'Cost',
      class: 'cost',
      icon: '',
      value: 0
    },
    {
      name: 'Closed Programs',
      class: 'closed',
      icon: '',
      value: 0
    }
  ];

  ngOnInit() {
    this.service.dashboard().subscribe((data: any) => {
      if(data !== null){
        this.dashboardItems[0].value = data.request;
        this.dashboardItems[1].value = data.certificate;
        this.dashboardItems[2].value = data.program;
        this.dashboardItems[3].value = data.enrollment;
        this.dashboardItems[4].value = data.cost;
        this.dashboardItems[5].value = data.closed;
      }
    });
  }


}
