import { NgModule } from '@angular/core';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/Shared.module';
import { EnrollmentService } from 'src/service/api/enrollment.service';
import { DetailComponent } from './detail/detail.component';
import { FeedbackService } from 'src/service/api/feedback.service';
import { AuthGuard } from 'src/service/security/AuthGarde';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title: 'Enrollment' ,breadCrum: 'View', role: 'canViewEnrollment' },
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: FormComponent,
    data: { title: 'Enrollment' ,breadCrum: 'Create', role: 'canAddEnrollment' },
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: { title: 'Enrollment' ,breadCrum: 'Edit', role: 'canUpdateEnrollment' },
    canActivate: [AuthGuard]
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    data: { title: 'Enrollment' ,breadCrum: 'Detail', role: 'canViewDetailEnrollment' },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [FormComponent,ListComponent, DetailComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    EnrollmentService,
    FeedbackService
  ],
  exports: [RouterModule]
})
export class EnrollmentModule { }
 