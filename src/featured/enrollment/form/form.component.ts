import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EnrollmentService } from 'src/service/api/enrollment.service';
import { FilteringEventArgs } from "@syncfusion/ej2-angular-dropdowns";
import { EmitType } from "@syncfusion/ej2-base";
import { Query } from "@syncfusion/ej2-data";
import { Location } from "@angular/common";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;

  public formGroup: FormGroup;
  public id: any;
  public formSubmitted = false;
  public isUpdate: boolean = false;

  public program: any;
  public employees: any;
  public fields: any = { text: 'text',value: 'value' };

  constructor(private activatedRoute: ActivatedRoute,private service: EnrollmentService, private location: Location,
    private router: Router) {
    this.formGroup = new FormGroup({ 
      id : new FormControl(null),
      employee_id : new FormControl({ value: '', disabled: true },Validators.required),
      program_id : new FormControl('',Validators.required),
    })
  }

  ngOnInit() {
    this.load_program();

    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id !== undefined) {
      this.isUpdate = true;
      this.service.get(this.id).subscribe((data: any) => {
        this.formGroup.setValue(data)
      });
    }

  }

  public getControls(name): FormControl {
    return this.formGroup.get(name) as FormControl;
  }

  Submit() {
    this.formSubmitted = true;
    if (!this.formGroup.valid) {
      return;

    } else {
      if (this.isUpdate) {
        this.service.update(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/enrollment'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      } else {
        this.service.create(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/enrollment'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      }
    }
  }

  load_program(){
    this.service.programs().subscribe((data: any) => {
      this.program = data;
    });
  }

  public onFilteringEmployees: EmitType<any> = (e: FilteringEventArgs) => {
    let query = new Query();
    query = e.text != "" ? query.where("full_name", "contains", e.text, true) : query;
    const filterInfo = { keyword: e.text, program_id: this.getControls('program_id').value };

    this.service.employees(filterInfo).subscribe((data: any) => {
      e.updateData(data);
    });

  };

  cancel() {
    this.location.back();
  }

}