import { SecurityService } from './../../../service/security/security.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FeedbackService } from 'src/service/api/feedback.service';
import { Location } from "@angular/common";

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;

  public formGroup: FormGroup;
  public id: any;
  public formSubmitted = false;
  public comments: any[] = [];
 
  constructor(private activatedRoute: ActivatedRoute,private service: FeedbackService, 
    private router: Router,public secu_service: SecurityService,private location: Location,) {
   
      this.formGroup = new FormGroup({ 
      employee_id : new FormControl(this.secu_service.logged_in_data.employee_id),
      comment : new FormControl('',[Validators.required]),
      enrollment_id : new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id !== undefined) {
      this.getControls('enrollment_id').setValue(this.id);
      this.service.gets(this.id).subscribe((data: any) => {
        this.comments = data;
      });
    }
  }

  public getControls(name): FormControl {
    return this.formGroup.get(name) as FormControl;
  }

  Submit() {
    this.formSubmitted = true;
    if (!this.formGroup.valid) {
      return;

    } else {
      this.service.create(this.formGroup.value).subscribe((response: any) => {
        if(response.status){
          this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
          this.ngOnInit();

        } else {
          this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
        
        }

      });
    }
  }

  delete(id,index) {
    if(confirm('are you sure want to delete this comment ?')){
      this.service.delete(id).subscribe((response: any) => {
        if (response.status) {
          this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
          this.comments.splice(index)
          
        } else {
          this.notif.Show({title: "Error",content: response.message,cssClass: "e-toast-danger" });
            
        }

      });
    }
  }

  cancel() {
    this.location.back();
  }

}  
