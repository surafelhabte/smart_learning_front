import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { FormGroup, FormControl } from '@angular/forms';
import { EnrollmentService } from 'src/service/api/enrollment.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;
  
  public formGroup: FormGroup;

  public data = [];
  public columns: any[] = [];
  public Command = ['edit', 'delete','detail']; 

  public placeholder: any = "-- Select Year to Filter Enrollement --";

  constructor(private service: EnrollmentService, private router: Router) {
    this.formGroup = new FormGroup({ fiscal_year: new FormControl(null) });
    this.prepare();
  }
  
  ngOnInit() {
    this.load_data(null);
  }

  add() {
    this.router.navigate(["enrollment/create"]);
  }

  edit($event) {
    this.router.navigate(["enrollment/edit/" + $event.id]);
  }

  detail($event) {
    this.router.navigate(["enrollment/detail/" + $event.id]);
  }

  delete($event) {
    this.service.delete($event.id).subscribe((response: any) => {
      if (response.status) {
        this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
        this.ngOnInit();
        
      } else {
        this.notif.Show({title: "Error",content: response.message,cssClass: "e-toast-danger" });
          
      }
    });
  }

  load_data(value){
   value !== null ? this.formGroup.get('fiscal_year').setValue(value.itemData.value) : null;

   this.service.filter(this.formGroup.value).subscribe((data: any[]) => {
      this.data = data;
   });

  }

  prepare() {
    this.columns.push({field: "full_name",headerText: "Full Name", textAlign: "center",width: 50 });
    this.columns.push({field: "sex",headerText: "Gender", textAlign: "center",width: 20 });
    this.columns.push({field: "position",headerText: "Position", textAlign: "center",width: 60 });
    this.columns.push({field: "department_name",headerText: "Department ", textAlign: "center",width: 60 });
    this.columns.push({field: "title",headerText: "Training", textAlign: "center",width: 60 });
    this.columns.push({field: "type",headerText: "Type", textAlign: "center",width: 30 });
  }

}
