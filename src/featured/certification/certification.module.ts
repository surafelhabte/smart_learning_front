import { AuthGuard } from './../../service/security/AuthGarde';
import { NgModule } from '@angular/core';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/shared/Shared.module';
import { CertificationService } from 'src/service/api/certification.service';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title:'Certification', breadCrum: 'View', role: 'canViewCertification' },
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: FormComponent,
    data: { title:'Certification', breadCrum: 'Create', role: 'canAddCertification' },
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: { title:'Certification', breadCrum: 'Edit', role: 'canUpdateCertification' },
    canActivate: [AuthGuard]
  }
];

@NgModule({
  declarations: [FormComponent,ListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    CertificationService,
  ],
  exports: [RouterModule]
})
export class CertificationModule { }
