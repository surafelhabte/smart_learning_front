import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { CertificationService } from 'src/service/api/certification.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;
  
  public formGroup: FormGroup;

  public data = [];
  public columns: any[] = [];
  public Command = ['edit', 'delete'];

  public placeholder: any = "-- Select Year to Filter Certification --";

  constructor(private service: CertificationService, private router: Router) {
    this.formGroup = new FormGroup({ fiscal_year: new FormControl(null) });
    this.prepare();
  }
  
  ngOnInit() {
    this.load_data(null);
  }

  add() {
    this.router.navigate(["certification/create"]);
  }

  edit($event) {
    this.router.navigate(["certification/edit/" + $event.id]);
  }

  delete($event) {
    this.service.delete($event.id).subscribe((response: any) => {
      if (response.status) {
        this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
        this.ngOnInit();
        
      } else {
        this.notif.Show({title: "Error",content: response.message,cssClass: "e-toast-danger" });
          
      }
    });

  }

  load_data(value){
   value !== null ? this.formGroup.get('fiscal_year').setValue(value.itemData.value) : null;

   this.service.filter(this.formGroup.value).subscribe((data: any[]) => {
      this.data = data;
   });

  }

  prepare() {
    this.columns.push({field: "full_name",headerText: "Full Name", textAlign: "center",width: 50 });
    this.columns.push({field: "sex",headerText: "Sex", textAlign: "center",width: 20 });
    this.columns.push({field: "position",headerText: "Position", textAlign: "center",width: 50 });
    this.columns.push({field: "department_name",headerText: "Department ", textAlign: "center",width: 50 });
    this.columns.push({field: "title",headerText: "Training", textAlign: "center",width: 50 });
    this.columns.push({field: "type",headerText: "Training Type", textAlign: "center",width: 40 });
  }
 
}