import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CertificationService } from 'src/service/api/certification.service';
import { EmitType } from "@syncfusion/ej2-base";
import { Query } from "@syncfusion/ej2-data";
import { FilteringEventArgs } from "@syncfusion/ej2-angular-dropdowns";
import { Location } from "@angular/common";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;

  public formGroup: FormGroup;
  public id: any;
  public formSubmitted = false;
  public isUpdate: boolean = false;

  public enrollments: any; 
  public employees: any;
  public fields: any = { text: 'text',value: 'value' };
  public certifications: any = [{text: 'Before The ECX', value: '1'},{text: 'After The ECX', value: '2'}]


  constructor(private activatedRoute: ActivatedRoute,private service: CertificationService, 
    private location: Location,private router: Router) {
    
      this.formGroup = new FormGroup({ 
      id : new FormControl(null),
      employee_id : new FormControl('',Validators.required),
      enrollment_id : new FormControl({ value: '', disabled: true }, Validators.required),
      result : new FormControl('',Validators.required),
      date : new FormControl('',Validators.required),
      training : new FormControl({ value: '', disabled: true },Validators.required),
      attchement : new FormControl('',Validators.required)
    })
  }
 
  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id !== undefined) {
      this.isUpdate = true;
      this.service.get(this.id).subscribe((data: any) => {
        this.formGroup.setValue(data)
      });
    }
  } 

  public getControls(name): FormControl {
    return this.formGroup.get(name) as FormControl;
  }

  Submit() {
    this.formSubmitted = true;
    if (!this.formGroup.valid) {
      return;

    } else {
      if (this.isUpdate) {
        this.service.update(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/certification'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }

        });
      } else {
        this.service.create(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/certification'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      }
    }
  }

  public onFilteringEmployees: EmitType<any> = (e: FilteringEventArgs) => {
    let query = new Query();
    query = e.text != "" ? query.where("full_name", "contains", e.text, true) : query;
    const filterInfo = { keyword: e.text };

    this.service.employee(filterInfo).subscribe((data: any) => {
      e.updateData(data);
    });

  }; 
  
  public onSelectEmployee(event){
    this.service.enrollment({ employee_id : event.itemData.value }).subscribe((data: any) => {
      this.enrollments = data;
    });
  }

  on_Select_Certification(value){
    switch(value.itemData.value){
      case '1':
      this.getControls('enrollment_id').disable();
      this.getControls('training').enable();
      break;

      case '2':
      this.getControls('enrollment_id').enable();
      this.getControls('training').disable();
      break;
    }

  }

  cancel() {
    this.location.back();
  }

  onAttachmentChange(event){
    if (event.target.files.length > 0) {
      const attchement = event.target.files[0];
      this.getControls('attchement').setValue(attchement);
    }
  }



}