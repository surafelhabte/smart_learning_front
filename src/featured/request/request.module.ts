import { NgModule } from '@angular/core';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { Routes, RouterModule } from '@angular/router';
import { RequestService } from 'src/service/api/request.service';
import { SharedModule } from 'src/shared/Shared.module';
import { AuthGuard } from 'src/service/security/AuthGarde';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title: 'Learning Request', breadCrum: 'View', role: 'canViewRequest' },
    canActivate: [AuthGuard]
  },
  {
    path: 'detail/:id',
    component: DetailComponent,
    data: { title: 'Learning Request', breadCrum: 'Detail', role: 'canViewDetailRequest' },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [DetailComponent,ListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    RequestService
  ],
  exports: [RouterModule]
})
export class RequestModule { }
 