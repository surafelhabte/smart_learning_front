import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/service/api/request.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {  
  public formGroup: FormGroup;

  public data = [];
  public columns: any[] = [];
  public Command = ['detail'];

  constructor(private service: RequestService, private router: Router) {
    this.formGroup = new FormGroup({ fiscal_year: new FormControl(null) });
    this.prepare();
  }

  ngOnInit() {
    this.load_data(null);
  }

  detail($event) {
    this.router.navigate(["request/detail/" + $event.id]);
  }

  load_data(value){
    value !== null ? this.formGroup.get('fiscal_year').setValue(value.itemData.value) : null;
 
    this.service.filter(this.formGroup.value).subscribe((data: any[]) => {
      this.data = data;
    });
  }

  prepare() {
    this.columns.push({field: "full_name",headerText: "Full Name", textAlign: "center",width: 50 });
    this.columns.push({field: "position",headerText: "Position", textAlign: "center",width: 50 });
    this.columns.push({field: "department_name",headerText: "Department ", textAlign: "center",width: 50 });
    this.columns.push({field: "location",headerText: "Duty Station", textAlign: "center",width: 50 });
    this.columns.push({field: "title",headerText: "Training", textAlign: "center",width: 50 });
    this.columns.push({field: "program_type",headerText: "Program Type", textAlign: "center",width: 50 });
    this.columns.push({field: "created_at",headerText: "Date", textAlign: "center",width: 50 });
 
  }

} 
 