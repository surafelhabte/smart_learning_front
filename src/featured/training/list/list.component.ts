import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { TrainingService } from 'src/service/api/training.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;
  
  public data = [];
  public columns: any[] = [];
  public Command = ['edit', 'delete'];

  constructor(private service: TrainingService, private router: Router) {
    this.prepare();
  }

  ngOnInit() {
    this.service.gets().subscribe((data: any[]) => {
      this.data = data;
    });
  }

  add() {
    this.router.navigate(["training/create"]);
  }

  edit($event) {
    this.router.navigate(["training/edit/" + $event.id]);
  }

  delete($event) {
    this.service.delete($event.id).subscribe((response: any) => {
      if (response.status) {
        this.notif.Show({ title: "Success", content: response.message,cssClass: "e-toast-success"});
        this.ngOnInit();
        
      } else {
        this.notif.Show({title: "Error",content: response.message,cssClass: "e-toast-danger" });
          
      }
    });
  }

  prepare() {
    this.columns.push({field: "title",headerText: "Training", textAlign: "center",width: 50 });
    this.columns.push({field: "type",headerText: "Type", textAlign: "center",width: 30 });
    this.columns.push({field: "importance",headerText: "Importance ", textAlign: "center",width: 30 });
    this.columns.push({field: "priority",headerText: "Priority", textAlign: "center",width: 30 });
    this.columns.push({field: "method",headerText: "Method Of Training", textAlign: "center",width: 30 });
 
  } 
  
}
