import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainingService } from 'src/service/api/training.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SharedToastComponent } from 'src/shared/shared-toast/shared-toast.component';
import { Location } from "@angular/common";


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild("notif") notif: SharedToastComponent;

  public formGroup: FormGroup;

  public id: any;
  public formSubmitted = false;
  public isUpdate: boolean = false;
 
  public method: any[] = ["short/long term courses","workshops","one to one coaching","conference/seminars",
                        "on-job training's","professional certifications"];

  public priority: any[] = ["First","Second","Third"]
  public importance: any[] = ["High","Medium","Low"]
  public type: any[] = ["Certification","Training"]

  constructor(private activatedRoute: ActivatedRoute,private service: TrainingService, private location: Location,
    private router: Router) {
    this.formGroup = new FormGroup({ 
      id : new FormControl(null),
      title : new FormControl('',Validators.required),
      description : new FormControl('',Validators.required),
      type : new FormControl('',Validators.required),
      importance : new FormControl('',Validators.required),
      priority : new FormControl('',Validators.required),
      method : new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    if (this.id !== undefined) {
      this.isUpdate = true;
      this.service.get(this.id)
      .subscribe((data: any) => {
        this.formGroup.setValue(data)
      });
    }
  }

  public getControls(name): FormControl {
    return this.formGroup.get(name) as FormControl;
  }

  Submit() {
    this.formSubmitted = true;
    if (!this.formGroup.valid) {
      return;

    } else {
      if (this.isUpdate) {
        this.service.update(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/training'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      } else {
        this.service.create(this.formGroup.value).subscribe((response: any) => {
          if(response.status){
            this.router.navigate(['/training'], {state: {data: {title: 'Success', content: response.message, cssClass:'e-toast-success'}}});
          
          } else {
            this.notif.Show({ title: "Error", content: response.message,cssClass: "e-toast-danger"});
          }
        });
      }
    }
  }

  cancel() {
    this.location.back();
  }

}
