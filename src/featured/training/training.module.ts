import { TrainingService } from './../../service/api/training.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormComponent } from './form/form.component';
import { ListComponent } from './list/list.component';
import { SharedModule } from 'src/shared/Shared.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/service/security/AuthGarde';

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    data: { title: 'Training', breadCrum: 'View', role: 'canViewTraining' },
    canActivate: [AuthGuard]
  },
  {
    path: 'create',
    component: FormComponent,
    data: { title: 'Training', breadCrum: 'Create', role: 'canAddTraining' },
    canActivate: [AuthGuard]
  },
  {
    path: 'edit/:id',
    component: FormComponent,
    data: { title: 'Training', breadCrum: 'Edit', role: 'canUpdateTraining' },
    canActivate: [AuthGuard]
  },
];

@NgModule({
  declarations: [FormComponent,ListComponent],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    TrainingService
  ],
  exports: [RouterModule]
})
export class TrainingModule { }
